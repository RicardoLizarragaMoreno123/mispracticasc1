package com.example.mispracticasc1;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class ingresaCotizacionActivity extends AppCompatActivity {
    /*
        private AppBarConfiguration appBarConfiguration;
        private ActivityIngresaCotizacionBinding binding;
    */
    private EditText txtUsuario;
    private Button btnIngresar, btnCerrar;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_ingresa_cotizacion);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtUsuario.getText().toString().matches("")){
                    Toast.makeText(ingresaCotizacionActivity.this,
                            "Falto capturar el usuario", Toast.LENGTH_SHORT).show();
                }
                else{

                    Intent intent = new Intent(ingresaCotizacionActivity.this, cotizacionActivity.class);
                    intent.putExtra("cliente", txtUsuario.getText().toString());
                    startActivity(intent);

                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



}